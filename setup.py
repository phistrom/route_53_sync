#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from setuptools import setup

setup(
    name='route-53-sync',
    version='0.1.1',
    packages=['route_53_sync'],
    install_requires=['boto'],
    license='MIT',
    author='Phillip Stromberg',
    author_email='phillip+r53sync@stromberg.me',
    description="Syncs Route 53 domains with this machine`s public IP",
    url="https://bitbucket.org/phistrom/route_53_sync",
    download_url="https://bitbucket.org/phistrom/route_53_sync/get/0.1.zip",
    scripts=['scripts/get_public_ip.py', 'scripts/sync_public_ip.py'],
    classifiers=[
            'Development Status :: 5 - Production/Stable',
            'License :: OSI Approved :: MIT License',
            'Operating System :: OS Independent',
            'Programming Language :: Python',
            'Programming Language :: Python :: 2.7',
            'Programming Language :: Python :: 3',
            'Programming Language :: Python :: 3.3',
            'Programming Language :: Python :: 3.4',
            'Programming Language :: Python :: 3.5',
            'Topic :: Internet',
            'Topic :: System :: Networking',
        ],
)
