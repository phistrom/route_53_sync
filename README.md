# Route 53 Sync
Sync a machine's public IP address with one or more domain names hosted on Amazon's Route 53.

  - Simple to use
  - Only requires [boto]

## Installation
`pip install route-53-sync`

## Example
Set `test.whatever.com` and `www.example.com` to this machine's public IP address:

`sync_public_ip.py test.whatever.com www.example.com`

## Version
0.1.0

## Requirements
* [boto]

License
----
MIT

   [boto]: <https://pypi.python.org/pypi/boto>
