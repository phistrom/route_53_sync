#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Sync your public IP with an arbitrary number of Route 53-hosted domain names.

Usage examples:

Set test.example.com and www.yourface.com to A records of this machine's public IP address:
`sync_public_ip.py test.example.com www.yourface.com`

Set test.example.com to your public IP address and if it already exists as a CNAME, silently replace it with an A
record instead:
`sync_public_ip.py -f test.example.com`

Return code reflects the number of domains that failed to update. If you specified 3 domains and got a return code
of 2, only 1 of the domains was set to your public IP.

by Phillip Stromberg
on 2016-10-09
"""

import argparse
import logging

from route_53_sync import get_public_ip, logger, Route53, Route53SyncError


def _get_arguments():
    parser = argparse.ArgumentParser('Sync with Route 53')
    parser.add_argument('-f', '--force', action='store_true',
                        help="Existing CNAME records of the same name will be deleted. Normally this would just log an "
                             "error and no change would be effected.")
    parser.add_argument('--access', help="Your AWS Access Key. You can omit this if you've performed "
                                         "`aws configure` on the command line.")
    parser.add_argument('--secret', help="Your AWS Secret Key. You can omit this if you've performed "
                                         "`aws configure` on the command line.")
    parser.add_argument('domains', nargs='+',
                        help="The A records you wish to create pointing to this machine's public IP address; one or "
                             "more domain names separated by spaces")
    return parser.parse_args()


def main():
    """
    Gets the command line arguments, your public IP, and then sets all the given domain names to that public IP.

    :return: the number of domains that failed to update (0 means all succeeded)
    """
    args = _get_arguments()
    ip = get_public_ip()
    r53 = Route53(access=args.access, secret=args.secret, forced_overwrites=args.force)
    retval = 0
    for domain in args.domains:
        try:
            r53.update_record(domain, ip)
            logger.info("Successfully updated %s to %s", domain, ip)
        except Route53SyncError as ex:
            logger.error(ex.message)
            retval += 1
    return retval


if __name__ == "__main__":
    logging.basicConfig()
    exit(main())  # return code is the number of errors encountered
