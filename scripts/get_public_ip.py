#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Print your public IP address

by Phillip Stromberg
on 2016-10-04
"""

from route_53_sync import get_public_ip


def main():
    """
    Prints your public IP address
    """
    print(get_public_ip())
    return 0


if __name__ == "__main__":
    exit(main())
