# -*- coding: utf-8 -*-
"""


by Phillip Stromberg
on 2016-10-09
"""


class Route53SyncError(Exception):
    def __init__(self, message=''):
        self.message = message


class ConflictingRecordError(Route53SyncError):
    pass


class ZoneNotFoundError(Route53SyncError):
    pass
