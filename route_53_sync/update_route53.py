#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
A class for conveniently updating Amazon Route 53 records.

by Phillip Stromberg
on 2016-10-09
"""


from boto import connect_route53
from . import logger
from route_53_sync.exc import ConflictingRecordError, ZoneNotFoundError
from time import sleep


class Route53(object):
    """
    A class for conveniently updating Amazon Route 53 records.
    """

    def __init__(self, access=None, secret=None, forced_overwrites=False):
        """
        Create a new connection to Amazon Route 53

        :param access: your AWS access key (not necessary if you ran `aws configure` from awscli)
        :param secret: your AWS secret key (not necessary if you ran `aws configure` from awscli)
        :param forced_overwrites: if True, CNAME records will automatically be converted to A records; otherwise an
                                    error is raised.
        """
        self._connection = connect_route53(aws_access_key_id=access, aws_secret_access_key=secret)
        self._zones = self._connection.get_zones()
        self.forced_overwrites = forced_overwrites

    def _find_matching_zone(self, domain_name):
        """
        Given a domain name, searches the Route 53 account for the longest zone name that is a subset of this
        domain name.

        For example, if given super.domain.awesome.com and the account contained both domain.awesome.com and
        awesome.com, then this function would return the Route 53 `Zone` object for domain.awesome.com because it is
        the longest.

        :param domain_name: a domain name to find amongst your Route 53 account's zones
        :return: the longest matching name's `Zone` object for the given `domain_name`
        """
        if not domain_name.endswith('.'):
            domain_name += '.'
        domain_name = domain_name.lower()
        matching = None
        longest_match = 0
        for z in self._zones:
            if domain_name.endswith(z.name) and len(z.name) > longest_match:
                matching = z
                longest_match = len(z.name)
        if matching:
            return matching
        else:
            raise ZoneNotFoundError("Couldn't find a zone in this AWS account for '%s' Some valid zones include: %s"
                                    % (domain_name, ', '.join([z.name for z in self._zones[:5]])))

    def _delete_record(self, zone, domain_name):
        """
        Deletes any A or CNAME record that matches the given domain_name.

        `domain_name` must be a fully-qualified domain name (FQDN). For example, if you want to delete the record
        'www' from the Zone 'example.com', you must pass 'www.example.com' as the `domain_name`, not just 'www'.

        :param zone: a Route 53 `Zone` object to delete the record from
        :param domain_name: the A or CNAME record to delete.
        """
        for method in (zone.delete_a, zone.delete_cname):
            try:
                method(domain_name)
                sleep(3)
            except AttributeError:
                pass  # if record didn't exist, it will be None
        else:
            logger.debug("Record %s was not deleted. Might not have existed." % domain_name)

    def update_record(self, domain_name, ip_addr):
        """
        Create or update a given domain name to be an A record pointing to the given IP address.

        :param domain_name: the domain name to be updated
        :param ip_addr: the IP address to set the domain name to point to
        """
        zone = self._find_matching_zone(domain_name)
        if self.forced_overwrites:
            self._delete_record(zone, domain_name)
            zone.add_a(domain_name, ip_addr)
            return
        # if not forced overwrite (default behavior)
        try:
            zone.add_a(domain_name, ip_addr)
        except:  # record may have already existed, just update it
            try:
                zone.update_a(domain_name, ip_addr)  # if this fails too, allow to raise up
            except AttributeError:
                raise ConflictingRecordError("%s already exists as an incompatible record. Use --force flag to "
                                             "replace this record anyway." % domain_name)
