#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Some IP tools like getting your public IP address.
That's about it I guess.

by Phillip Stromberg
on 2016-10-04
"""

try:
    from urllib.request import urlopen
    PY3 = True
except ImportError:
    from urllib2 import urlopen
    PY3 = False


def get_public_ip():
    """
    Retrieve your public IP from an external site and return it.

    :return: This machine's public IP address.
    """
    if PY3:
        with urlopen('https://myip.stromberg.me/text/') as response:
            text = response.read()
            ip = text.strip().decode('utf-8')
    else:
        stream = urlopen('https://myip.stromberg.me/text/')
        text = stream.read()
        stream.close()
        ip = text.strip()
    return ip
