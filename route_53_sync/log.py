#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""


by Phillip Stromberg
on 2016-10-09
"""

import logging

LOGGER_NAME = 'route_53_sync'

logger = logging.getLogger(LOGGER_NAME)
logger.setLevel(logging.INFO)
