# -*- coding: utf-8 -*-
"""

by Phillip Stromberg
on 2016-10-04
"""

from .log import logger
from .exc import Route53SyncError
from .ip_tools import get_public_ip
from .update_route53 import Route53
